<?php

/**
 * medicalalert functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package medicalalert
 */

if (!function_exists('medicalalert_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function medicalalert_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on medicalalert, use a find and replace
         * to change 'medicalalert' to the name of your theme in all the template files.
         */
        load_theme_textdomain('medicalalert', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            // 'menu-1' => esc_html__( 'Primary', 'medicalalert' ),
            'primary' => esc_html__('Primary', 'medicalalert'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('medicalalert_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */

        add_theme_support('custom-logo');
        function medicalalert_custom_logo_setup()
        {
            $defaults = array(
                'height'      => 100,
                'width'       => 400,
                'flex-height' => true,
                'flex-width'  => true,
                'header-text' => array('site-title', 'site-description'),
            );
            add_theme_support('custom-logo', $defaults);
        }

        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1400, 9999);
    }
endif;
add_action('after_setup_theme', 'medicalalert_setup');

function medicalalert_add_editor_style()
{
    add_editor_style('css/customStyleSheet.css');
}
add_action("admin_init", 'medicalalert_add_editor_style');
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function medicalalert_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('medicalalert_content_width', 2272);
}
add_action('after_setup_theme', 'medicalalert_content_width', 0);

/**
 * Register widget area.
 */

/**
 * Enqueue scripts and styles.
 */
function medicalalert_scripts()
{
    wp_enqueue_style('medicalalert-style', get_stylesheet_uri());

    wp_enqueue_script('medicalalert-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('medicalalert-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    wp_enqueue_script('medicalalert-nav-scroll', get_stylesheet_directory_uri() . '/js/nav-scroll.js', array('jquery'), null, true);
    wp_enqueue_script('medicalalert-mobile-header', get_stylesheet_directory_uri() . '/js/mobile-header.js', array('jquery'), null, true);
    wp_enqueue_script('medicalalert-hover-effect', get_stylesheet_directory_uri() . '/js/hover-effect.js', array('jquery'), null, true);

    // Adding Bootstrap css
    wp_enqueue_style("medicalalert-bs-css", get_template_directory_uri() . '/css/bootstrap.min.css');

    wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), null, true);

    wp_enqueue_style('load-fa', 'https://use.fontawesome.com/releases/v5.11.2/css/all.css');

    wp_enqueue_style("medicalalert-fa-css", get_template_directory_uri() . '/fonts/font-awesome.min.css');
    wp_enqueue_style("medicalalert-custom-css", get_template_directory_uri() . '/css/customStyleSheet.css');

    wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', false, '', true);
    wp_enqueue_script('popper');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'medicalalert_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Widgets File.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Navigation(bootstrap Navwalker) File.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';


/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}
