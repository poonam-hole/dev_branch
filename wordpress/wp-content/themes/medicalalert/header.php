<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package medicalalert
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<section class="sectionClass">
		<div class="container">
			<div class="row">
				<div class="col-md-12 no-padding-class">
					<div class="header">
						<div class="headerText">
							<label class="text-style-1">Senior Citizens Day Offer from</label>
							$19.95! One FREE Month
							<label class="text-style-1">and</label> FREE Shipping*
						</div>
					</div>
				</div>
				<div class="col-md-12 no-padding-class">
					<div id="page" class="site">
						<a class="skip-link screen-reader-text" href="#content">
							<?php esc_html_e('Skip to content', 'medicalalert'); ?></a>

						<header id="masthead" class="site-header desktop-header">
							<nav id="menu" class="navbar navbar-expand-md no-padding-class" role="navigation">

								<div class="site-branding navbar-brand">
									<a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?>
										<img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/malogo-tag-450.png" class="desktop-logo" />
									</a>
								</div>
								<!-- <button class="navbar-toggler navbar-toggler-right justify-content-center" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation"> -->
								<!-- <span class="navbar-toggler-icon"></span> -->
								<!-- </button> -->
								<?php
								wp_nav_menu(array(
									'menu' => 'primary',
									'theme-location' => 'primary',
									'container' => 'div',
									'container_id' => 'bs4navbar',
									'container_class' => 'collapse navbar-collapse',
									'menu_id' => 'main-menu',
									'menu_class' => 'navbar-nav ml-auto',
									'fallback_cb' => 'bs4navwalker::fallback',
									'walker' => new bs4navwalker(),
								));
								?>
							</nav>
							<button type="button" class="btn order-now-btn">Order Now</button>
							<div class="contact-number">800-800-2537</div>

							<!-- #site-navigation -->
						</header><!-- #masthead -->

					</div>
				</div>
			</div>

			<header class="row mobile-header">
				<div class="col-2 col-sm-2 text-center">
					<img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/menu.png" class="openMenu" />
				</div>
				<div class="col-8 col-sm-8 text-center">
					<img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/malogo-450.png" />
				</div>
				<div class="col-1 col-sm-1 no-padding-class">
					<img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/call.png" />
				</div>
				<div class="col-1 col-sm-1 no-padding-class">
					<img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/icon-cart.png" />
				</div>
			</header>

		</div>
		</div>
		</div>
		<div class="side-menu">
			<img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/shape@2x.png" class="close-menu" />
			<?php
			wp_nav_menu(array(
				'menu' => 'primary',
				'theme-location' => 'primary',
				'container' => 'div',
				'container_id' => 'bs4navbar',
				'container_class' => '',
				'menu_id' => '',
				'menu_class' => '',
				'fallback_cb' => 'bs4navwalker::fallback',
				'walker' => new bs4navwalker(),
			));
			?>
		</div>
	</section>
	<div id="content" class="site-content section">