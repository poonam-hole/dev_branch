<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package medicalalert
 */

?>

</div><!-- #content -->

<div class="container-fluid">
	<footer id="colophon" class="site-footer row">
		<div class="site-info col-md-12 margin-bottom-15">
			<div class="row call-us-at">
				<div class="call-us-at-div">
					<div class="Call-us-at-800-800-2 paddingTop20 ">Call us at <label class="text-style-1"> 800-800-2537</label>
					</div>
					<div class="or paddingTop20 ">or</div>
					<button type="button" class="btn call-us-at-order-now-btn border-0"><label class="Order-Now no-margin-class">Order Now</label></button>
				</div>
			</div>
			<div class="row desktop-footer">
				<div class="offset-md-1 col-md-10 border-bottom padding-bottom-25">
					<div class="row">
						<div class="col-md-4 padding-top-32">
							<div class="About-Medical-Alert">About Medical Alert
							</div>
							<div class="Medical-Alert-by-Con">
								Medical Alert by Connect America provides Medical Alert Systems to older
								adults with medical ailments or conditions, who wish to live at home.
								Connect America is a nationwide company, with its corporate headquarters
								located just outside of Philadelphia, Pennsylvania.</div>
						</div>

						<div class="col-md-4 padding-top-32">
							<div class="Company">Company</div>

							<div class="row">
								<div class="col-md-4 no-padding-class About-Us-Careers-Cus">
									<ul class="no-margin-class padding-left-15">
										<li>About Us</li>
										<li>Careers</li>
										<li>Customer Care</li>
										<li>Safety Tips</li>
									</ul>
								</div>
								<div class="col-md-6 no-padding-class">
									<ul class="no-margin-class padding-left-15">
										<li class="Partnership-Opportun">Partnership Opportunities</li>
										<li class="Partnership-Opportun">Healthcare Providers</li>
										<li class="Fraud-Abuse-or-Neg">Submit Incident Report (Fraud, Abuse or Neglect)</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-4 padding-top-32">
							<div class="Products-Services">Products & Services
							</div>
							<div class="row">
								<div class="col-md-6 no-padding-class -Monitoring-At-H">
									<ul class="no-margin-class padding-left-15">
										<li>24/7 Monitoring</li>
										<li>At Home Landline</li>
										<li>At Home No Landline</li>
										<li>On the Go</li>
									</ul>
								</div>
								<div class="col-md-6 no-padding-class Fall-Detection-FAQ-M">
									<ul class="no-margin-class padding-left-15">
										<li>Fall Detection</li>
										<li>FAQ</li>
										<li>Medical Alert Support</li>
										<li>Alert911</li>
									</ul>
								</div>
							</div>
						</div>

					</div>
					<div class="row padding-bottom-20 padding-top-32">
						<div class="Connect-with-Us col-md-12">Connect with Us
						</div>

						<div class="social-media-images">
							<span class="facebook" id="facebook"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/facebook.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/facebook-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/facebook.png'" /></s>

								<span class="youtube padding-left-15" id="youtube"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/youtube.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/youtube-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/youtube.png'" /></span>

								<span class="linked-in padding-left-15" id="linked-in"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/linked-in.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/linked-in-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/linked-in.png'" /></span>

								<span class="twitter padding-left-15" id="twitter"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/twitter.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/twitter-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/twitter.png'" /></span>
						</div>
					</div>

				</div>
				<div class="offset-md-1 col-md-11 borderTop">
					<div class="site-info row padding-top-20 copy-right padding-bottom-25">
						<div class="col-md-7 no-padding-class">©2019 Medical Alert &nbsp; Privacy Policy &nbsp; Terms and Conditions</div>
						<div class="col-md-5">v1.26.0.20190715</div>
					</div><!-- .site-info -->
				</div>
			</div>
			<div class="row mobile-footer">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-12 about_medical">
							<div class="collapsible-div borderBottom">
								<a href="#about_medical" data-toggle="collapse">
									<div class="About-Medical-Alert">About Medical Alert
										<div class="dropdown_down"></div>
									</div>
								</a>
							</div>
							<div class="Medical-Alert-by-Con collapse" id="about_medical">
								Medical Alert by Connect America provides Medical Alert Systems to older
								adults with medical ailments or conditions, who wish to live at home.
								Connect America is a nationwide company, with its corporate headquarters
								located just outside of Philadelphia, Pennsylvania.</div>
						</div>
						<div class="col-sm-12">
							<div class="collapsible-div borderBottom">
								<a href="#company" data-toggle="collapse">
									<div class="Company">Company
										<div class="dropdown_down"></div>
									</div>
								</a>
							</div>
							<div class="row collapse" id="company">
								<div class="col-sm-12 no-padding-class About-Us-Careers-Cus">
									<ul class="no-margin-class padding-left-15">
										<li>About Us</li>
										<li>Careers</li>
										<li>Customer Care</li>
										<li>Safety Tips</li>
										<li>Partnership Opportunities</li>
										<li>Healthcare Providers</li>
										<li>Submit Incident Report (Fraud, Abuse or Neglect)</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="collapsible-div borderBottom">
								<a href="#products_services" data-toggle="collapse">
									<div class="Products-Services">Products & Services
										<div class="dropdown_down"></div>
									</div>
								</a>
							</div>
							<div class="row collapse" id="products_services">
								<div class="col-sm-12 no-padding-class -Monitoring-At-H">
									<ul class="no-margin-class padding-left-15">
										<li>24/7 Monitoring</li>
										<li>At Home Landline</li>
										<li>At Home No Landline</li>
										<li>On the Go</li>
										<li>Fall Detection</li>
										<li>FAQ</li>
										<li>Medical Alert Support</li>
										<li>Alert911</li>
									</ul>
								</div>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 connect_us">
							<div class="collapsible-div borderBottom"><a href="#connect_us" data-toggle="collapse">
									<div class="Connect-with-Us">Connect with Us
										<div class="dropdown_down"></div>
									</div>
								</a>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="social-media-images collapse" id="connect_us">
										<span class="facebook" id="facebook"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/facebook.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/facebook-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/facebook.png'" /></s>

											<span class="youtube" id="youtube"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/youtube.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/youtube-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/youtube.png'" /></span>

											<span class="linked-in" id="linked-in"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/linked-in.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/linked-in-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/linked-in.png'" /></span>

											<span class="twitter" id="twitter"><img src="http://172.24.11.239/wordpress/wp-content/uploads/2019/10/twitter.png" onmouseover="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/twitter-copy.png'" onmouseout="this.src='http://172.24.11.239/wordpress/wp-content/uploads/2019/10/twitter.png'" /></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row borderTop">
						<div class="site-info row copy-right padding-bottom-25">
							<div class="col-sm-6 col-6 padding-left-25">©2019 Medical Alert </div>
							<div class="col-sm-5 col-5 padding-left-25">Privacy Policy </div>
							<div class="col-sm-6 col-6 padding-left-25">Terms and Conditions</div>
							<div class="col-sm-5 col-5 padding-left-25">v1.26.0.20190715</div>
						</div><!-- .site-info -->
					</div>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #container -->
</div><!-- #page -->


<?php wp_footer(); ?>

</body>

</html>