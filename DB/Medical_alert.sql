select * from wordpress.wp_options;
select * from wordpress.wp_posts;

SET SQL_SAFE_UPDATES = 0;

UPDATE  wordpress.wp_options SET option_value = replace(option_value, 'http://172.24.11.239/wordpress', 'http://172.24.12.1/MedicalAlert/wordpress') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE  wordpress.wp_posts SET post_content = replace(post_content, 'http://172.24.11.239/wordpress', 'http://172.24.12.1/MedicalAlert/wordpress');

UPDATE  wordpress.wp_postmeta SET meta_value = replace(meta_value,'http://172.24.11.239/wordpress', 'http://172.24.12.1/MedicalAlert/wordpress');

UPDATE  wordpress.wp_usermeta SET meta_value = replace(meta_value,'http://172.24.11.239/wordpress', 'http://172.24.12.1/MedicalAlert/wordpress');

UPDATE  wordpress.wp_links SET link_url = replace(link_url, 'http://172.24.11.239/wordpress', 'http://172.24.12.1/MedicalAlert/wordpress');

UPDATE  wordpress.wp_comments SET comment_content = replace(comment_content , 'http://172.24.11.239/wordpress', 'http://172.24.12.1/MedicalAlert/wordpress');